import React,{useState}from 'react';
import {View, Text, Image, ImageBackground} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';

import style from './style';

export default function SignUpScreen(props) {

  const [emailData,setEmailData] = useState(props.route.params.emailObj)
  const [passwordData,setPasswordData] = useState(props.route.params.passwordObj)

  

  return (
    <View style={style.containor}>
      <ImageBackground
        source={require('../../Assets/loginBack.png')}
        style={style.containor}>
        <View style={style.blanckView}></View>
        <View style={style.imageView}>
          <Image
            source={require('../../Assets/Logo1.png')}
            style={style.comLogo}
          />
        </View>
        <View style={style.textView}>
          <Text style={style.textStyle}>{"Login with your email address"}</Text>
          <Text style={style.textStyleOne}>{"which is connected to your membership"} </Text>
        </View>
        <View style={style.blanckViewOne}></View>
        <View style={style.textInputView}>
           <Text style={style.emailTextStyle}>{"Email Address : "}{emailData}</Text>
        </View>
        <View style={style.textInputView}>
          <Text style={style.emailTextStyle}>{"Password :"} {passwordData}</Text>
        </View>

        <View style={style.forgotView}>
          <TouchableOpacity style={style.loginButtonStyle}>
            <Text style={style.emailTextStyle}>{"Login"}</Text>
          </TouchableOpacity>
          <View style={style.forgotPasswordStyle}>
            <Text style={style.textStyleOne}>{"ForgotPassword"}</Text>
            <Text style={style.signUpTextStyle}>{"SignUp"}</Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}
