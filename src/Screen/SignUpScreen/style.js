import {StyleSheet, ImageBackground} from 'react-native';

export default StyleSheet.create({
  containor: {
    height: '100%',
    width: '100%',
  },
  blanckView: {
    height: '10%',
    width: '100%',
  },
  imageView: {
    height: '12%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  comLogo: {
    height: '60%',
    width: '70%',
  },
  textView: {
    height: '13%',
    width: '70%',
    marginTop: '3%',
    alignSelf: 'center',
  },
  textStyle: {
    color: 'white',
    fontSize: 20,
  },
  textStyleOne: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  blanckViewOne: {
    height: '1%',
    width: '100%',
  },
  textInputView: {
    height: '9%',
    width: '70%',
    alignSelf: 'center',
    marginTop: '6%',
  },
  emailTextStyle: {
    color: 'white',
    fontSize: 18,
  },
  inputStyle: {
    backgroundColor: 'white',
    height: '60%',
    marginTop: '2%',
    borderRadius: 5,
    paddingLeft: 5,
  },
  loginButtonStyle: {
    height: '40%',
    width: '70%',
    marginTop: '20%',
    alignSelf: 'center',
    backgroundColor: '#ED724C',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
  },
  forgotView: {
    height: '20%',
    width: '40%',
    alignSelf: 'center',
  },
  forgotPasswordStyle: {
    height: '50%',
    width: '100%',
    marginTop: '20%',
  },
  signUpTextStyle: {
    color: 'white',
    fontSize: 18,
    marginTop: '8%',
    textAlign: 'center',
  },
});
