import React, { useState, useRef } from 'react';
import { View, Text, Image, ImageBackground,Keyboard } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';

import style from './style';



export default function LoginScreen(props) {

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [Isapp_loding, setIsapp_loding] = useState(false);

  const emailRef = useRef();
  const passwordlRef = useRef();

  const SignUpNavigation = () => {
    props.navigation.navigate("SignUpScreen", {
      emailObj: email,
      passwordObj: password
    }
    )
  }


  const onLoginClick = async () => {
    Keyboard.dismiss();

    const isConnected = await NetworkCheck.isNetworkAvailable();

    if (isConnected) {
        if (email === "") {
            setEmail("");
            MYDROP.alert('error', "email cannot be blank");
            return
        }
        if (email.length > 0) {
            let expression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (expression.test(email)) { }
            else {
                setEmail("")
                MYDROP.alert('error', "email format is invalid");
                return
            }
        }
        if (password === '') {
           setPassword("")
            MYDROP.alert('error', "password cannot be blank");
            return
        }
        if (password.length < 5) {
            MYDROP.alert('error', "password should contain atleast 6 letters");
            return
        }

        const form = new FormData();
        form.append("email", email)
        form.append("password", password)

        try {
            setIsapp_loding(true)
            const { data } = await AuthServices.LoginUser(form)

            if (data.status == 0) {
                setIsapp_loding(false)
                MYDROP.alert('error', 'Invalid Number or password', "Try Again");
            }

            if (data.status == 1) {
                setIsapp_loding(true)
                var value = JSON.stringify(data);
                   await AsyncStorage.setItem('User', value).then(
                    
                    props.navigation.replace('Home')
              )
           }
        }
        catch (error) {
            console.log(error)
            setIsapp_loding(false)
            console.log(error.data)
            MYDROP.alert('error', " Server Error : 500 ");
        }
    }
    else {
        MYDROP.alert('error', 'No Internet Connection', "please check your device connection");
    }

}

  return (
    <View style={style.containor}>
      <ImageBackground
        source={require('../../Assets/loginBack.png')}
        style={style.containor}>
        <View style={style.blanckView}></View>
        <View style={style.imageView}>
          <Image
            source={require('../../Assets/Logo1.png')}
            style={style.comLogo}
          />
        </View>
        <View style={style.textView}>
          <Text style={style.textStyle}>{"Login with your email address"}</Text>
          <Text style={style.textStyleOne}>{"which is connected to your membership"}</Text>
        </View>
        <View style={style.blanckViewOne}></View>
        <View style={style.textInputView}>
          <Text style={style.emailTextStyle}>{"Email Address"}</Text>
          <TextInput
            placeholder="email"
            ref={emailRef}
            style={style.inputStyle}
            maxLength={20}
            placeholderTextColor="white"
            onChangeText={setEmail}
            value={email}
            keyboardType="default"
          />
        </View>
        <View style={style.textInputView}>
          <Text style={style.emailTextStyle}>{"Password"}</Text>
          <TextInput
            placeholder="Password"
            ref={passwordlRef}
            style={style.inputStyle}
            maxLength={20}
            placeholderTextColor="white"
            onChangeText={setPassword}
            value={password}
            keyboardType="default"
          />
        </View>

        <View style={style.forgotView}>
          <TouchableOpacity onPress={SignUpNavigation} style={style.loginButtonStyle}>
            <Text style={style.emailTextStyle}>{"Login"}</Text>
          </TouchableOpacity>
          <View style={style.forgotPasswordStyle}>
            <Text style={style.textStyleOne}>{"ForgotPassword"}</Text>
            <Text style={style.signUpTextStyle}>{"SignUp"}</Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}
